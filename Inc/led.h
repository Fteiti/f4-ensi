#include "stm32f4xx_hal.h"

typedef struct{
	uint8_t status;
	GPIO_TypeDef*  port;
	uint16_t pin;
        __IO uint32_t timeout;
        uint32_t period, duty;
}ledStruct;

void LED_LowLevelInit(GPIO_TypeDef*  port,uint16_t pin);
void LED_init(ledStruct * led, GPIO_TypeDef*  port,uint16_t pin, uint32_t period, uint32_t duty);

void LED_routines(ledStruct * led);
void LED_decrementDelay(ledStruct * led);



