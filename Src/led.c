#include "led.h"

void LED_LowLevelInit(GPIO_TypeDef*  port,uint16_t pin){
  
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(port,pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(port, &GPIO_InitStruct);
}

void LED_init(ledStruct * led, GPIO_TypeDef*  port,uint16_t pin, uint32_t period, uint32_t duty){
  led->port=port;
  led->pin=pin;
  LED_LowLevelInit(led->port,led->pin);
  led->period=period;
  led->duty=duty;
  led->timeout=0;
}

void LED_Toggle(ledStruct * led){
  HAL_GPIO_TogglePin(led->port,led->pin);
}

void LED_routines(ledStruct * led){
  if(led->timeout==0){
    led->timeout=led->period;
    HAL_GPIO_WritePin(led->port,led->pin,GPIO_PIN_SET);
  }else if(led->timeout<led->period-led->duty){
    HAL_GPIO_WritePin(led->port,led->pin,GPIO_PIN_RESET);
  }
}

void LED_decrementDelay(ledStruct * led){
  if(led->timeout)
    led->timeout--;
}







